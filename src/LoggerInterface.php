<?php

namespace AlekhnovichWpbit\Logger;

interface LoggerInterface
{
    public static function log($context);
    public static function error($context);
    public static function method($classFile);
}