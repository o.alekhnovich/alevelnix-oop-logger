<?php

namespace AlekhnovichWpbit\Logger;

/**
 * Class Logger
 */
class Logger implements LoggerInterface
{
    public static function log($context)
    {
        self::method(require 'config.php')->writeLog(__FUNCTION__, $context);
    }

    public static function error($context)
    {
        self::method(require 'config.php')->writeLog(__FUNCTION__, $context);
    }

    public static function method($classFile)
    {
        $listenerClass = 'AlekhnovichWpbit\\Logger\\Methods\\' . ucfirst($classFile) . 'Class';
        if (class_exists($listenerClass)) {
            return $listenerClass::getInstance();
        } else {
            exit("This class does not exist! $classFile \n $listenerClass");
        }
    }
}