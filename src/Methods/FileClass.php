<?php
namespace AlekhnovichWpbit\Logger\Methods;

use AlekhnovichWpbit\Logger\Singleton;

class FileClass extends Singleton implements MethodInterface
{
    protected $fileHandle;

    protected function __construct()
    {
        $this->fileHandle = fopen('log.txt', 'a+');
    }

    protected function __destruct()
    {
        fclose($this->fileHandle);
    }

    public function writeLog($level, $message)
    {
        $date = date('Y-m-d G:i:s');
        $str = print_r($message, true);
        fwrite($this->fileHandle, "{$date} | {$level} | {$str} \n");
    }
}