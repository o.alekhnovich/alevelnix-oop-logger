<?php

namespace AlekhnovichWpbit\Logger\Methods;

/**
 * Interface MethodInterface
 */
interface MethodInterface
{
    public function writeLog($level, $message);
}